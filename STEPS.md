```sh
npm install --save-dev webpack webpack-cli webpack-dev-server cross-env
```

```sh
touch webpack.config.js
```

```sh
npm install --save-dev @babel/core @babel/preset-env @babel/preset-react babel-loader
```

```sh
touch babel.config.js
```

```sh
touch .browserslistrc
```

```sh
npm install core-js
```

```sh
npm install --save-dev css-loader style-loader sass-loader node-sass
```

```sh
npm install --save-dev postcss-loader autoprefixer
```

```sh
npm install --save-dev file-loader
```

```sh
npm install --save-dev webpack-dev-server
```
