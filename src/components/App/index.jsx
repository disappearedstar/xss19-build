import React from "react";
import Link from "../Link";
import logo from "./logo.svg";
import "./style.scss";

function App() {
  return (
    <div className="App">
      <header className="App__header">
        <img src={logo} className="App__logo" alt="logo" />
        <Link
          href="https://school.xsolla.com/dev_course"
          isExternal
          className="App__link"
        >
          Xsolla Summer School 2019
        </Link>
      </header>
    </div>
  );
}

export default App;
