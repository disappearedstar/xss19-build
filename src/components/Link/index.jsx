import React from "react";

const Link = ({ href, isExternal = false, children, ...props }) => (
  <a
    {...props}
    href={href}
    target={isExternal ? "_blank" : undefined}
    rel={isExternal ? "noopener noreferrer" : undefined}
  >
    {children}
  </a>
);

export default Link;
